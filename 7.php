<?php
class Animal{
    public $name;
    public $speck;
    public  $is_tail;
    public  $leg;
    public function __construct($n,$s,$i,$l){
        $this->name=$n;
        $this->speck=$s;
        $this->is_tail=$i;
        $this->leg=$l;
    }
}
$dog = new Animal('dog', 'bark', true, 4); // name, speak, is_tail, leg
print($dog->name) ;
    echo'<br>';
    print($dog->speck) ;
    echo'<br>';
    print($dog->is_tail) ;
    echo'<br>';
    print($dog->leg) ;
    echo'<hr>';
	$cat = new Animal('cat', 'meow', true, 4);
    
    print($cat->name);
    echo'<br>';
    print($cat->speck) ;
    echo'<br>';
    print($cat->is_tail) ;
    echo'<br>';
    print($cat->leg) ;
    echo'<hr>';
	$human = new Animal('human', 'human language', false, 2);
    print($human->name);
    echo'<br>';
    print($human->speck) ;
    echo'<br>';
    print($human->is_tail) ;
    echo'<br>';
    print($human->leg) ;
?>